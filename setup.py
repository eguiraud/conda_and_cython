from setuptools import setup, find_packages, Extension

setup(
    name="conda_and_cython",
    packages=find_packages(),
    zip_safe=False,
    ext_modules=[Extension("conda_and_cython.helper", ["conda_and_cython/helper.pyx"])]
)
