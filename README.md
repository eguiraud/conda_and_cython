# Dummy project to test conda distribution of a python package that makes use of cython

Usage:

```
$ git clone https://gitlab.com/bluehood/conda_and_cython
$ cd conda_and_cython
$ conda env create
$ conda activate cython_playground
$ conda build .
```
